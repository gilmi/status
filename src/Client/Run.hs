{-# LANGUAGE ScopedTypeVariables, OverloadedStrings #-}

module Client.Run where

import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString (sendAll, recv)
import qualified Net.IP as IP
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.Async as Async
import System.Process.Typed (shell, readProcess)
import System.Exit

import Types

run :: String -> Int -> FilePath -> IO (Maybe Response)
run addr port process = withSocketsDo $ do
  (code, out, err) <- readProcess $ shell process
  let
    msg
      | code == ExitSuccess =
        Success $ T.decodeUtf8 $ BSL.toStrict out
      | ExitFailure code' <- code =
        Failure
          code'
          (T.decodeUtf8 $ BSL.toStrict out)
          (T.decodeUtf8 $ BSL.toStrict err)
  sendMsg addr port msg


sendMsg :: String -> Int -> Msg -> IO (Maybe Response)
sendMsg addr port msg = withSocketsDo $ do
    addrinfos <- getAddrInfo Nothing (Just addr) (pure $ show port)
    let serverAddr = head addrinfos
    sock <- socket (addrFamily serverAddr) Stream defaultProtocol
    connect sock (addrAddress serverAddr)
    sendAll sock $ msgToBS msg
    (response' :: Maybe Response) <- parseMsg <$> recv sock 4096
    close sock
    pure response'
