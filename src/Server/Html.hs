{-# LANGUAGE LambdaCase, OverloadedStrings, RecordWildCards #-}

module Server.Html where

import Control.Arrow ((>>>))
import qualified Lucid as H
import Lucid.Html5
import Control.Monad
import Data.Text (Text, pack, unpack)
import qualified Data.Text as T
import Data.Monoid
import Data.List (find)
import Data.Time.Format

import Types
import Server.Types

type Html = H.Html ()


-- | A page template
template :: Text -> Html -> Html
template title body =
  doctypehtml_ $ do
    head_ $ do
      meta_ [ charset_ "utf-8" ]

      title_ (H.toHtml $ title)

      meta_ [ name_ "viewport", content_ "width=device-width, initial-scale=1" ]

      link_ [ rel_ "stylesheet", type_ "text/css", href_ "/assets/css/style.css" ]

    body_ $ do
      div_ [class_ "container"] $ do
        H.div_ [ H.class_ "top row" ] $ do
          header_ $ do
            h1_ (H.a_ [ H.href_ "/" ] "Status")

        div_ [id_ "main"] body

statusPage :: Bool -> T.Text -> Status -> Html
statusPage ispage name s@Status{..} = do
  H.div_ [class_ $ (if ispage then "" else "status ") <> statusClass s] $ do
    (if ispage then id else H.a_ [ H.href_ $ "/" <> name ]) $
      H.ul_ [class_ "status_info"] $ do
        H.li_ [class_ "status_name"] $ H.toHtml name
        H.li_ [class_ "status_time"] $
          H.toHtml $ formatTime defaultTimeLocale "%F %T" $ sTime
        H.li_ [class_ "status_msg"]  (msgToHtml sMsg)

statuses :: [(T.Text, Status)] -> Html
statuses = mapM_ (uncurry $ statusPage False)

statusClass :: Status -> T.Text
statusClass = sMsg >>> \case
  Success{} -> "success"
  Timeout{} -> "timeout"
  _ -> "failure"

msgToHtml :: Msg -> Html
msgToHtml = \case
  Timeout msg ->
    H.ul_ [class_ "status_msg"] $ do
      H.li_ [class_ "status_type"] $ H.toHtml ("Timeout" :: T.Text)
      msgToHtml msg
  Success msg -> do
    H.li_ [class_ "status_type"] $ H.toHtml ("Success" :: T.Text)
    H.p_ [class_ "success_msg"] $ H.pre_ $ H.toHtml msg
  Failure code out err -> do
    H.ul_ [class_ "status_msg"] $ do
      H.li_ [class_ "status_type"] $ H.toHtml ("Failure" :: T.Text)
      H.li_ [class_ "failure_code"]
        $ H.pre_ $ H.toHtml ("exit code: " <> show code)
      H.li_ [class_ "failure_stdout"]
        $ H.pre_ $ H.toHtml ("stdout: " <> out)
      H.li_ [class_ "failure_stderr"]
        $ H.pre_ $ H.toHtml ("stderr: " <> err)
