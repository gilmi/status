{-# LANGUAGE BangPatterns #-}

module Server.Types where

import Control.Concurrent.STM
import qualified Data.Map as M
import qualified Data.Text as T
import Control.Concurrent.STM.TVar
import Net.IP
import Data.Time

import Types

initState :: IO State
initState = do
  State
    <$> newTVarIO M.empty
    <*> newTQueueIO
    <*> newTVarIO M.empty

data State = State
  { sHub :: TVar Hub
  , sLog :: Log
  , sAliases :: TVar (M.Map IP T.Text)
  }

type Hub = M.Map IP Status

data Status = Status
  { sTime :: !ZonedTime
  , sMsg  :: !Msg
  }
  deriving Show

type Log = TQueue (ZonedTime, LogMsg)

data LogMsg
  = ClientMsg !IP !Msg
  | ClientAlias !IP !T.Text
  | Event !T.Text
  | UnknownClient !T.Text
  | BadMsg !IP !T.Text
  | Error !T.Text
  | TimedOut !IP !Msg
  deriving Show

type Logger = (ZonedTime, LogMsg) -> IO ()
