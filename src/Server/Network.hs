{-# LANGUAGE OverloadedStrings, ViewPatterns #-}

module Server.Network where

import Control.Exception
import Data.Monoid
import Control.Monad
import Network.Socket hiding (send, recv)
import Network.Socket.ByteString
import qualified Net.IP as IP
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.Async as Async
import Control.Concurrent (forkFinally)
import Data.Aeson hiding (Error, Success)
import Data.Time
import System.IO

import Types
import Server.Types

run :: Int -> State -> [Logger] -> IO ()
run port state loggers = do
  Async.race_
    (runLogger loggers $ sLog state)
    (netSocketServer port state)

netSocketServer :: Int -> State -> IO ()
netSocketServer port state = do
  addrInfos <-
    getAddrInfo
    (Just defaultHints{addrFlags = [AI_PASSIVE]})
    Nothing
    (Just $ show port)
  let serverAddr = head addrInfos
  bracket
    (socket (addrFamily serverAddr) Stream defaultProtocol)
    (const (toLog state $ Event "Closed") <=< close) $ \sock -> do
      setSocketOption sock ReuseAddr 1
      bind sock (addrAddress serverAddr)
      listen sock 1
      toLog state $ Event "Waiting for connections..."
      void $ forever $ do
        (soc, _) <- accept sock
        forkFinally
          (listener soc state)
          (const $ close soc)

listener :: Socket -> State -> IO ()
listener sock state = do
  msg' <- recv sock 4096
  addr <- takeWhile (/=':') . show <$> getPeerName sock
  time <- getZonedTime

  case (IP.decode (T.pack addr), parseMsg msg') of
    (Just ip, Just (Alias name)) -> do
      toLog state (ClientAlias ip name)
      STM.atomically $ do
        aliases <- STM.readTVar (sAliases state)
        STM.writeTVar (sAliases state) (M.insert ip name aliases)
      sendAll sock $ BSL.toStrict $ encode Ok
    (Just ip, Just msg) -> do
      toLog state (ClientMsg ip msg)
      STM.atomically $ do
        hub <- STM.readTVar (sHub state)
        STM.writeTVar (sHub state) (M.insert ip (Status time msg) hub)
      sendAll sock $ BSL.toStrict $ encode Ok

    (Just ip, Nothing) -> do
      toLog state (BadMsg ip $ T.decodeUtf8 msg')
      sendAll sock $ BSL.toStrict $ encode (Err "bad message")

    _ -> do
      toLog state (UnknownClient $ T.decodeUtf8 msg')
      sendAll sock $ BSL.toStrict $ encode (Err "unrecognized ip")

runLogger :: [Logger] -> Log -> IO ()
runLogger processes log = forever $ do
  msg <- STM.atomically $ STM.readTQueue log
  mapM_
    ($ msg)
    $ map ((flip catch $ \(SomeException e) -> hPutStrLn stderr (show e)) .) processes

toLog :: State -> LogMsg -> IO ()
toLog state msg = do
  time <- getZonedTime
  STM.atomically . flip STM.writeTQueue (time, msg) . sLog $ state
