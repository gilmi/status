{-# LANGUAGE TemplateHaskell, LambdaCase, OverloadedStrings, TupleSections #-}

module Server.Run where

import Web.Spock
import Web.Spock.Config
import Web.Spock.Lucid
import qualified Lucid as L

import Data.Tuple
import Data.Foldable
import Control.Arrow ((&&&), first)
import Control.Applicative
import Control.Monad
import Control.Monad.Trans
import Data.Maybe
import Data.Monoid
import Control.Concurrent (threadDelay)
import Control.Concurrent.STM
import Control.Concurrent.Async
import Net.IP
import qualified Net.IP as IP
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Map as M
import qualified Data.ByteString as BS
import qualified Network.Mime as Mime (defaultMimeLookup)
import Data.FileEmbed
import Data.Time


import qualified Server.Network as N (run)
import Types
import Server.Types
import Server.Html


assetsDir :: [(FilePath, BS.ByteString)]
assetsDir = $(embedDir "assets")

data Session = EmptySession

run :: Int -> Int -> Maybe Float -> [Logger] -> IO ()
run serverPort webPort timeout loggers = do
  ref <- initState
  concurrently_
    (forever $ logTimeout timeout ref)
    $ race_
      (N.run serverPort ref loggers)
      $ do
        spockCfg <- defaultSpockCfg EmptySession PCNoDatabase ref
        runSpock webPort (spock spockCfg $ app timeout)

app :: Maybe Float -> SpockM () Session State ()
app timeout = do
  for_ assetsDir $ \(path_, file_) -> do
    get (static $ "assets/" <> path_) $
      serveBytes path_ file_

  get root $ do
    (hub, aliases) <- liftIO . getMaps timeout =<< getState
    lucid
      . template "Status"
      . statuses
      . map (first $ getName aliases)
      . M.toList
      $ hub

  get var $ \ipOrAlias -> do
    (hub, aliases) <- liftIO . getMaps timeout =<< getState
    case getNameStatus hub aliases =<< (lookupByAlias aliases ipOrAlias <|> IP.decode ipOrAlias) of
      Nothing ->
        lucid $ template (T.pack $ show ipOrAlias) $ L.toHtml ("Not found." :: T.Text)
      Just (name, msg) ->
        lucid $ template name $ statusPage True name msg

serveBytes :: MonadIO m => FilePath -> BS.ByteString -> ActionT m a
serveBytes path file' = do
  let mime = Mime.defaultMimeLookup (T.pack path)
  setHeader "content-type" $ T.decodeUtf8 mime
  bytes file'


getNameStatus hub aliases ip =
  (getName aliases ip,) <$> M.lookup ip hub

getName aliases ip = fromMaybe (IP.encode ip) (M.lookup ip aliases)

lookupByAlias :: M.Map IP T.Text -> T.Text -> Maybe IP
lookupByAlias aliases name = lookup name $ map swap $ M.toList aliases

getMaps :: Maybe Float -> State -> IO (Hub, M.Map IP T.Text)
getMaps timeout State{sHub = h, sAliases = a, sLog = log} = do
  now <- getZonedTime
  atomically $ do
    ali <- readTVar a
    hub <- readTVar h
    hub' <- flip M.traverseWithKey hub $ curry $ \case
      (ip, Status t msg@Success{})
        | Just to <- fmap (*60) timeout
        , realToFrac to < (zonedTimeToUTC now) `diffUTCTime` (zonedTimeToUTC t)
        -> do
          writeTQueue log (now, TimedOut ip msg)
          pure $ Status t (Timeout msg)
      (_, s) -> pure s
    writeTVar h hub'
    pure (hub', ali)

logTimeout :: Maybe Float -> State -> IO ()
logTimeout timeout state = case timeout of
  -- threadDelay takes microseconds (0.000001 of a second)
  Nothing -> threadDelay (1000000 * 60 * 60 * 60)
  Just to -> do
    threadDelay $ ceiling $
      -- the thread will run every at least 10 seconds and at most 3 minutes
      maximum
        [ 1000000 * 60 * 10
        , minimum [1000000 * 60 * to / 4, 1000000 * 60 * 3]
        ]
    void $ getMaps timeout state
