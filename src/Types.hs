{-# LANGUAGE ScopedTypeVariables, LambdaCase, OverloadedStrings #-}

module Types where

import qualified Data.Text as T
import Data.Aeson hiding (Error, Success)
import qualified Data.Aeson.Types as A
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL

data Msg
  = Failure Int T.Text T.Text
  | Timeout Msg
  | Success T.Text
  | Alias T.Text
  deriving Show

data Response
  = Ok
  | Err T.Text
  deriving Show

instance ToJSON Response where
  toJSON = \case
    Ok ->
      object
        [ "type" .= String "ok"
        ]
    Err err ->
      object
        [ "type" .= String "error"
        , "error" .= String err
        ]

instance FromJSON Response where
  parseJSON = \case
    Object v -> do
      v .: "type" >>= \case
        ("ok" :: T.Text) ->
          pure Ok
        ("error" :: T.Text) ->
          Err <$> v .: "error"
    m -> A.typeMismatch "" m

instance ToJSON Msg where
  toJSON = \case
    Alias name ->
      object
        [ "type" .= String "alias"
        , "name" .= String name
        ]
    Success msg ->
      object
        [ "type" .= String "success"
        , "message" .= String msg
        ]
    Failure code stdout stderr ->
      object
        [ "type" .= String "failure"
        , "code" .= Number (fromIntegral code)
        , "stdout" .= String stdout
        , "stderr" .= String stderr
        ]

instance FromJSON Msg where
  parseJSON = \case
    Object v -> do
      v .: "type" >>= \case
        ("alias" :: T.Text) ->
          Alias <$> v .: "name"
        ("success" :: T.Text) ->
          Success <$> v .: "message"
        ("failure" :: T.Text) ->
          Failure
            <$> v .: "code"
            <*> v .: "stdout"
            <*> v .: "stderr"
    m -> A.typeMismatch "" m


parseMsg :: FromJSON a => BS.ByteString -> Maybe a
parseMsg = decode . BSL.fromStrict

msgToBS :: ToJSON a => a -> BS.ByteString
msgToBS = BSL.toStrict . encode
