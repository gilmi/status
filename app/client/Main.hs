{-# LANGUAGE OverloadedStrings, LambdaCase #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}  -- One more extension.
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}  -- To derive Show
{-# LANGUAGE TypeOperators      #-}

module Main where

import Data.Maybe
import Options.Generic
import qualified Data.Text as T
import Data.Maybe
import Net.IP
import System.IO
import System.Exit

import Types hiding (Alias)
import qualified Types as T
import Client.Run

data Config w
  = Run
  { address :: String <?> "Address of the server"
  , port :: Int <?> "Port of the message server"
  , process :: FilePath <?> "Process to run"
  }
  | Alias
  { address :: String <?> "Address of the server"
  , port :: Int <?> "Port of the message server"
  , name :: T.Text <?> "Your new alias"
  }
  deriving (Generic)

instance ParseRecord (Config Wrapped)
deriving instance Show (Config Unwrapped)

main :: IO ()
main = do
  cmd <- unwrapRecord "Report client"
  result <- case cmd of
    Run{} ->
      run (unHelpful $ address cmd) (unHelpful $ port cmd) (unHelpful $ process cmd)
    Alias{} ->
      sendMsg (unHelpful $ address cmd) (unHelpful $ port cmd) (T.Alias $ unHelpful $ name cmd)
  case result of
    Just Ok ->
      exitSuccess
    Just (Err err) -> do
      hPutStrLn stderr $ T.unpack err
      exitWith $ ExitFailure 2
    Nothing -> do
      hPutStrLn stderr "No response."
      exitWith $ ExitFailure (-1)
