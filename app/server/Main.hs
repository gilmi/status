{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}  -- One more extension.
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}  -- To derive Show
{-# LANGUAGE TypeOperators      #-}

module Main where

import Data.Maybe
import Options.Generic
import Server.Run


data Config w = Config
  { serverPort :: Int <?> "Port for the message server"
  , webPort :: Int <?> "Port for the web server"
  , timeout :: Maybe Float <?> "Status timeout (in minutes)"
  , logFile :: Maybe FilePath <?> "Log file"
  }
  deriving (Generic)

instance ParseRecord (Config Wrapped)
deriving instance Show (Config Unwrapped)

main :: IO ()
main = do
  config <- unwrapRecord "Collector server"
  run
    (unHelpful $ serverPort config)
    (unHelpful $ webPort config)
    (unHelpful $ timeout config)
    $ catMaybes
      [ Just print
      , (. (snoc '\n') . show) . appendFile <$> unHelpful (logFile config)
      ]

snoc :: a -> [a] -> [a]
snoc x = reverse . (x:) . reverse
